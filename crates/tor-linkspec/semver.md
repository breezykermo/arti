MODIFIED: New ByRelayIds type.
BREAKING: Changed the semantics of HasAddrs
BREAKING: ChanTarget now requires HasChanMethod
MODIFIED: RelayIdRef now implements Hash.

